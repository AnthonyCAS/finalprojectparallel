package Jugadores;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Juego.Board;
import Juego.Move;
import Juego.TicTacToe;

//clase tipo player para que podamos jugar con la maquina via std, falta gui
public class PlayerHuman implements Player {

	// pedir coordenadas para jugar
	private byte getCoord() {

		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		byte n = -1;
		try {
			n = Byte.parseByte(in.readLine());
		} catch (final IOException e) {
			System.out.println("Problemas al leer!");
			return getCoord();//volvemos a pedir
		} catch (final NumberFormatException e) {
			System.out.println("No es un n�mero!");
			return getCoord(); //volvemos a pedir
		}

		if ((n < 0) || (n >= Board.SIZE)) {
			System.out
					.println("No en el rango (0-" + (Board.SIZE - 1) + ")!");
			return getCoord();//volvemos a pedir
		}
		return n;

	}

	//hacer jugada humano
	public Move getMove(final byte[][] board, final Move prev, final byte player) {
		Board.printBoard(board);
		// ask for move
		System.out.println("Tu ficha es: " + TicTacToe.resolvePiece(player)
				+ ", Selecciona tu movimiento:");
		System.out.print("Pos X ? ");
		final byte x = getCoord();
		System.out.print("Pos Y ? ");
		final byte y = getCoord();

		return new Move(x, y, player);

	}

}