package Jugadores;

import Juego.Board;
import Juego.Move;
import Juego.TicTacToe;
//clase para entrenar, hace jugadas logicas, la uso para que juegue contra la maquina durante el entrenamiento
public class Entrenador implements Player {
	//verifico si puedo ganar , en filas columnas y diagonales
	private Move canWin(final byte board[][], final byte color) {
		//columnas
		for (int i = 0; i < 3; i++) {
			final Move move1 = new Move((byte) 0, (byte) i, color);
			final Move move2 = new Move((byte) 1, (byte) i, color);
			final Move move3 = new Move((byte) 2, (byte) i, color);
			final Move move = findWin(board, move1, move2, move3, color);
			if (move != null) {
				return move;
			}
		}
		//filas
		for (int i = 0; i < 3; i++) {
			final Move move1 = new Move((byte) i, (byte) 0, color);
			final Move move2 = new Move((byte) i, (byte) 1, color);
			final Move move3 = new Move((byte) i, (byte) 2, color);
			final Move move = findWin(board, move1, move2, move3, color);
			if (move != null) {
				return move;
			}
		}
		//diagonales
		Move move1 = new Move((byte) 0, (byte) 0, color);
		Move move2 = new Move((byte) 1, (byte) 1, color);
		Move move3 = new Move((byte) 2, (byte) 2, color);
		Move move = findWin(board, move1, move2, move3, color);
		if (move != null) {
			return move;
		}

		move1 = new Move((byte) 2, (byte) 0, color);
		move2 = new Move((byte) 1, (byte) 1, color);
		move3 = new Move((byte) 0, (byte) 2, color);
		move = findWin(board, move1, move2, move3, color);
		if (move != null) {
			return move;
		}

		return null;
	}
	// como estrategia si esta libre inicio en el centro
	private Move centerOpen(final byte[][] board, final byte color) {
		final Move center = new Move((byte) 1, (byte) 1, color);
		if (Board.isEmpty(board, center)) {
			return center;
		} else {
			return null;
		}
	}
	// como estrategia si esta libre inicio en una esquina
	private Move cornerOpen(final byte[][] board, final byte color) {
		final Move corner1 = new Move((byte) 0, (byte) 0, color);
		final Move corner2 = new Move((byte) 2, (byte) 2, color);
		final Move corner3 = new Move((byte) 2, (byte) 0, color);
		final Move corner4 = new Move((byte) 0, (byte) 2, color);

		if (Board.isEmpty(board, corner1)) {
			return corner1;
		} else if (Board.isEmpty(board, corner2)) {
			return corner2;
		} else if (Board.isEmpty(board, corner3)) {
			return corner3;
		} else if (Board.isEmpty(board, corner4)) {
			return corner4;
		} else {
			return null;
		}
	}
	//verifico si hay un tres en raya
	private Move findWin(final byte[][] board, final Move move1, final Move move2, final Move move3, final int color) {
		if ((board[move1.x][move1.y] == color)
				&& (board[move2.x][move2.y] == color)
				&& (board[move3.x][move3.y] == TicTacToe.EMPTY)) {
			return move3;
		} else if ((board[move2.x][move2.y] == color)
				&& (board[move3.x][move3.y] == color)
				&& (board[move1.x][move1.y] == TicTacToe.EMPTY)) {
			return move1;
		} else if ((board[move1.x][move1.y] == color)
				&& (board[move3.x][move3.y] == color)
				&& (board[move2.x][move2.y] == TicTacToe.EMPTY)) {
			return move2;
		} else {
			return null;
		}
	}
	//ia hace un movimiento
	public Move getMove(final byte[][] board, final Move prev, final byte color) {
		//no esta full continuo
		if (Board.isFull(board)) {
			return null;
		}

		Move result = null;
		//ficha enemiga
		final byte other = TicTacToe.reverse(color);
		//veo si puedo ganar
		if (result == null) {
			result = canWin(board, color);
		}
		//en otro caso veo que el enemigo puede ganar
		if (result == null) {
			result = canWin(board, other);
		}
		//obtengo un move

		//si no puede ganar mi enemigo ni yo empiezo
		if (result == null) {
			result = centerOpen(board, color);
		}

		if (result == null) {
			result = cornerOpen(board, color);
		}

		// si no puedo abrir en centro, esquina, ni ganar, ni mi enemigo  
		if (result == null) {
			result = moveSomewhere(board, color);
		}
		
		if (result.color() != color) {
			//System.out.println("enemigo x: "+ result.x+" y: "+result.y);
			result = new Move(result.x, result.y, color);
		}

		return result;
	}
	//mover donde sea, esta logica no tiene estrategia
	public Move moveSomewhere(final byte[][] board, final byte player) {

		for (;;) {
			final byte x = (byte) (Math.random() * 3.0); 
			final byte y = (byte) (Math.random() * 3.0); 
			final Move m = new Move(x, y, player);
			if (Board.isEmpty(board, m)) {
				return m;
			}
		}

	}

}