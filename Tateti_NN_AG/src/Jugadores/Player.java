package Jugadores;

import Juego.Move;


public interface Player { // clase abstracta, tipo base de los demas

	//hace el siguiente movimietno segun el tipo de jugador, recibe anterior jugada
	public Move getMove(byte[][] board, Move prev, byte color);

}