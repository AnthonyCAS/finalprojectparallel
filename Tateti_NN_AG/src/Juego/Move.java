package Juego;
//clase move, que contiene la informacion dela jugada, coordenadas y tipod e jugador en la variable color
public class Move {

	// tipo de jugador, inicializo con vacio
	private byte _color = TicTacToe.EMPTY;
	public byte x = -1;
	public byte y = -1;

	//movimiento
	public Move(final byte x, final byte y, final byte color) {
		this.x = x;
		this.y = y;
		this._color = color;
	}

	//obtener tipo de jugador
	public byte color() {
		return this._color;
	}

	//atajo para imprimir con std, para pruebas
	public String toString() {
		return this.getClass().getName() + " (" + this.x + ", " + this.y
				+ ") of color " + this._color;
	}

}