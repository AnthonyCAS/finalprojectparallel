package Juego;

public class Board {

	// tama�o del juego, actualmetne solo soporta tres en raya
	public static final byte SIZE = 3;

	// funcion apra copiar un tablero
	public static byte[][] copy(final byte[][] board) {
		//una unica asignacion  a newboard
		final byte[][] newBoard = new byte[board.length][board[0].length];
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				newBoard[x][y] = board[x][y];
			}
		}
		return newBoard;
	}

	//determina si el casillero esta vacio, recibe un move
	public static boolean isEmpty(final byte[][] board, final Move move) {
		return (board[move.x][move.y] == TicTacToe.EMPTY);
	}

	//determina si el tablero esta full, no mas movimeitnos
	public static boolean isFull(final byte[][] board) {
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board[0].length; y++) {
				if (board[x][y] == TicTacToe.EMPTY) {
					return false;
				}
			}
		}
		return true;
	}

	//determina si hay un ganador
	public static boolean isWinner(final byte[][] board, final byte player) {
		//cpor filas
		for (int x = 0; x < board.length; x++) {
			if ((board[x][0] == player) && (board[x][1] == player)
					&& (board[x][2] == player)) {
				return true;
			}
		}
		//por columnas
		for (int y = 0; y < board[0].length; y++) {
			if ((board[0][y] == player) && (board[1][y] == player)
					&& (board[2][y] == player)) {
				return true;
			}
		}

		// Diagonales
		if ((board[0][0] == player) && (board[1][1] == player)
				&& (board[2][2] == player)) {
			return true;
		}
		if ((board[0][2] == player) && (board[1][1] == player)
				&& (board[2][0] == player)) {
			return true;
		}

		return false;
	}

	//hacemos un movimiento
	public static boolean placePiece(final byte[][] board, final Move m) {

		if (board[m.x][m.y] != TicTacToe.EMPTY) {
			throw new IllegalStateException("Selecciona otro casillero diferente a: (" + m.x + ", " + m.y + ")");
		} else {
			board[m.x][m.y] = m.color();
			return true;
		}
	}

	//imprimo tablero, no podre usar gui, necesito mas tiempo xd
	public static void printBoard(final byte[][] board) {

		System.out.print("  ");
		for (int i = 0; i < board.length; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
		for (int x = 0; x < board.length; x++) {
			System.out.print(" ");
			for (int i = 0; i < board[0].length; i++) {
				System.out.print("--");
			}
			System.out.println("- ");
			System.out.print(x + "|");
			for (int y = 0; y < board.length; y++) {
				if (board[x][y] == TicTacToe.EMPTY) {
					System.out.print(" |");
				} else if (board[x][y] == TicTacToe.NOUGHTS) {
					System.out.print("o|");
				} else if (board[x][y] == TicTacToe.CROSSES) {
					System.out.print("x|");
				} else {
					throw new IllegalStateException("Casillero (" + x + ", " + y
							+ ") no est� vacio, error!");
				}
			}
			System.out.println(x);
		}
		System.out.print(" ");
		for (int i = 0; i < board.length; i++) {
			System.out.print("--");
		}
		System.out.println("- ");
		System.out.println(" ");
		System.out.print("  ");
		for (int i = 0; i < board.length; i++) {
			System.out.print(i + " ");
		}
	}
}

