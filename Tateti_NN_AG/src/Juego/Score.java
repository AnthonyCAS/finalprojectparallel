package Juego;
import logical.tateti;
import Jugadores.Player;
//clase importante, para calcular el valor de pesos en una red neuronal o simplement epara calcular el score de u jugador
public class Score {
	public final static int WIN_POINTS = 50;
	public final static int DRAW_POINTS = 20;
	public final static int LOSE_POINTS = 1;

	public final static double PERFECT_SCORE = tateti.SCORE_TAM	* WIN_POINTS;
	Player player; 
	Player rival;
	boolean flag_info; // para testear, true me muestra información por consola

	public Score(final Player player, final Player rival, final boolean flag_info) {
		this.player = player;
		this.rival = rival;
		this.flag_info = flag_info;
	}
	//calcular score, apra ello se usa tres valores, la victoria pesa mas que perder y empatar 50 20 1
	public double score() {
		int win = 0;
		int lose = 0;
		int draw = 0;

		for (int i = 0; i < tateti.SCORE_TAM; i++) {
			//creamos jugadores auxiliares (copia)
			final Player players[] = new Player[2];
			players[0] = this.player;
			players[1] = this.rival;
			// instanciamos un juego
			final Game game = new Game(players);
			//creamos y obtenemos al ganador
			final Player winner = game.play();
			
			if (this.flag_info) {
				System.out.println("Juego #" + i + ":" + winner);
				game.printBoard();
			}
			//contamos victorias, derrotas o empates para el actual jugador
			if (winner == this.player) {
				win++;
			} else if (winner == this.rival) {
				lose++;
			} else {
				draw++;
			}
		}

		if (this.flag_info) {
			System.out.println("Win:" + win);
			System.out.println("Lose:" + lose);
			System.out.println("Draw:" + draw);
		}

		final double score = (win * WIN_POINTS) + (lose * LOSE_POINTS) + (draw * DRAW_POINTS);

		final double s = (PERFECT_SCORE - score) / PERFECT_SCORE; //promedio de victorias, score 0 a 1
		//nunca sucedera pero por si acaso
		if (s < 0) {
			return -1;
		}
		return s;

	}
}
