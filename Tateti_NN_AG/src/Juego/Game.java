package Juego;
import Jugadores.Player;;
//logica del juego, turnos, etc
public class Game {

	// tablero del juego
	private final byte[][] _board = new byte[3][3];

	//jugador, siempre inicia vacio
	private byte _player = TicTacToe.NOUGHTS;

	// jugadores, estoy definiendo tipos de jugadores:
	//humano, maquina el cual puede usar redes neuronales movidas random
	//solo deben existir 2 jugadores
	private Player[] _players = new Player[2];
	// inicializo el tablero.
	{
		for (int x = 0; x < this._board.length; x++) {
			for (int y = 0; y < this._board[0].length; y++) {
				this._board[x][y] = TicTacToe.EMPTY;
			}
		}
	}

	//constructor, creo jugadores.
	 
	public Game(final Player[] players) {
		if (players.length != 2) {
			throw new IllegalArgumentException("Error no hay dos jugadores");
		}
		this._players = players;
	}

	// funcion para comenzar el juego, se llama primero
	public Player play() {
		return play(null);
	}

	// funcion para jugar luego de la primera movida, iterativo usando move para validar, retorna al ganador
	private Player play(Move prev) {
		//bucle infinito hasta encontrar al ganador
		for (;;) {
			final byte[][] b = Board.copy(this._board); //copiamos el tablero
			final Move move = this._players[this._player].getMove(b, prev, this._player);
			//llamamos a la funcion getmove para realzia run movimiento, esta funcion es polimorfica por que cada tipo de player 
			//juega de diferente forma usando el mismo nombre como funcion

			if (move == null) {
				throw new NullPointerException("Error al mover, checkear");
			}

			// check square is empty
			if (!Board.isEmpty(this._board, move)) {
				System.out.println("El casillero no esta vacio, intenta otro!");
				play(prev);
			}

			// jugar o excepion
			if (!Board.placePiece(this._board, move)) {
				System.out.println("Movimiento ilegal, intenta de nuevo!");
				play(prev);
			}
			//en el caso que se hizo la jugada, se verifica si hay un ganador
			if (Board.isWinner(this._board, this._player)) {
				return this._players[this._player];
			}

			// cambiar turno
			this._player = TicTacToe.reverse(this._player);
			//empate
			if (Board.isFull(this._board)) {
				//System.out.print("empate");
				return null;
			}
			//actualizamos movimiento
			prev = move;
		}

	}

	public void printBoard() {
		Board.printBoard(this._board);

	}
}
