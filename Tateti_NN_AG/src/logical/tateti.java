package logical;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Executors;

import Juego.Game;
import Juego.Score;
import Jugadores.*;
//import com.heatonresearch.book.introneuralnet.ch6.TicTacToe.players.neural.PlayerNeural;
//import com.heatonresearch.book.introneuralnet.ch6.TicTacToe.players.neural.TicTacToeGenetic;
//import com.heatonresearch.book.introneuralnet.neural.feedforward.FeedforwardLayer;
//import com.heatonresearch.book.introneuralnet.neural.feedforward.FeedforwardNetwork;
//import com.heatonresearch.book.introneuralnet.neural.util.SerializeObject;


public class tateti {

	public final static int POPULATION_SIZE = 200;
	public final static double MUTATION_PERCENT = 0.10;
	public final static double MATE_PERCENT = 0.25;
	public final static int NEURONS_HIDDEN_1 = 10;
	public final static int NEURONS_HIDDEN_2 = 0;
	public final static int TRAIN_MINUTES = 1;
	public final static int SCORE_TAM = 100; //para el score, cantidad de juegos 
	public final static int THREAD_POOL_SIZE = 2;

//	public static FeedforwardNetwork createNetwork() {
//		final FeedforwardNetwork network = new FeedforwardNetwork();
//		network.addLayer(new FeedforwardLayer(9));
//		network
//				.addLayer(new FeedforwardLayer(tateti.NEURONS_HIDDEN_1));
//		if (tateti.NEURONS_HIDDEN_2 > 0) {
//			network.addLayer(new FeedforwardLayer(
//					tateti.NEURONS_HIDDEN_2));
//		}
//		network.addLayer(new FeedforwardLayer(1));
//		network.reset();
//		return network;
//	}
//
	public static Player getPlayer(final String name) throws IOException,
			ClassNotFoundException {
		if (name.equalsIgnoreCase("Human")) {
			return new PlayerHuman();
//		} else if (name.equalsIgnoreCase("NeuralLoad")) {
			//return new PlayerNeural(loadNetwork());
//		} else if (name.equalsIgnoreCase("NeuralBlank")) {
			//return new PlayerNeural(createNetwork());
		} else if (name.equalsIgnoreCase("Logic")) {
			return new Entrenador();
		} else {
			return null;
		}
	}
//
//	public static FeedforwardNetwork loadNetwork() throws IOException,
//			ClassNotFoundException {
//		final FeedforwardNetwork result = (FeedforwardNetwork) SerializeObject
//				.load("tictactoe.net");
//		return result;
//	}



	public static void main(final String[] args) {
		System.out.println("Juego de Tatteti usando Redes Neuronales, se uso algoritmos genéticos como entrenamiento!" ) ; 
		try {
			final tateti ttt = new tateti();

//			if (args.length < 3) {
//				System.out
//						.println("Usage: tateti [game/match/train] [player1] [player2]");
//				System.exit(0);
//			}

			final Player player1 = getPlayer("Human");
			final Player player2 = getPlayer("Logic");

			if (player1 == null) {
				System.out.println("Error Jugador 1.");
				System.exit(0);
			}

			if (player2 == null) {
				System.out.println("Error Jugador 2.");
				System.exit(0);
			}

			ttt.setPlayer1(player1);
			ttt.setPlayer2(player2);

//			if (args[0].equalsIgnoreCase("game")) {
				ttt.JuegoSimple();
//			} else if (args[0].equalsIgnoreCase("match")) {
//				ttt.playMatch();
//			} else if (args[0].equalsIgnoreCase("train")) {
				//ttt.geneticNeural();
//			} else {
//				System.out.println("Invalid mode.");
//			}
		} catch (final Throwable t) {
			t.printStackTrace();
		}
	}


private Player player1;
private Player player2;
//
//	public void geneticNeural() throws IOException {
//		final FeedforwardNetwork network = createNetwork();
//		// train the neural network
//		System.out.println("Determining initial scores");
//		final TicTacToeGenetic train = new TicTacToeGenetic(network, true,
//				tateti.POPULATION_SIZE,
//				tateti.MUTATION_PERCENT, tateti.MATE_PERCENT,
//				this.player2.getClass());
//		train.setPool(Executors
//				.newFixedThreadPool(tateti.THREAD_POOL_SIZE));
//		int epoch = 1;
//
//		final Date started = new Date();
//		Date now = new Date();
//		int minutes = 0;
//		do {
//			train.iteration();
//			System.out.println("Epoch #" + epoch + " Error:" + train.getScore()
//					+ ",minutes left="
//					+ (tateti.TRAIN_MINUTES - minutes));
//			epoch++;
//			now = new Date();
//			minutes = (int) (now.getTime() - started.getTime()) / 1000;
//			minutes /= 60;
//		} while (minutes < tateti.TRAIN_MINUTES);
//
//		train.getPool().shutdownNow();
//		SerializeObject.save("tictactoe.net", train.getNetwork());
//	}
//
//	public void playMatch() {
//		final Player[] players = new Player[2];
//		players[0] = this.player1;
//		players[1] = this.player2;
//		final ScorePlayer score = new ScorePlayer(players[0], players[1], true);
//		System.out.println("Score:" + score.score());
//	}
//
//	public void playNeural() throws IOException, ClassNotFoundException {
//		final FeedforwardNetwork network = loadNetwork();
//
//		final Player[] players = new Player[2];
//		players[0] = new PlayerNeural(network);
//		players[1] = new PlayerMinMax();
//		final ScorePlayer score = new ScorePlayer(players[0], players[1], true);
//		score.score();
//
//	}
//  asignar jugadores
	public void setPlayer1(final Player player1) {
		this.player1 = player1;

	}

	public void setPlayer2(final Player player2) {
		this.player2 = player2;

	}

	public void JuegoSimple() {
		Game game = null;
		final Player[] players = new Player[2];
		players[0] = this.player1;
		players[1] = this.player2;
		game = new Game(players);
		final Player winner = game.play();
		if(winner == null)
			System.out.println("Empate");
		else
			System.out.println("El ganador es:" + winner);
	}
}